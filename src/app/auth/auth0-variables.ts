interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: '1GHK0jFDHstDIjiaTBWfKfLFL0AN8nJ1',
  domain: 'abhijeetb.auth0.com',
  callbackURL: 'http://localhost:3000/home'
};
