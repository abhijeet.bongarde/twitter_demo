export class User {
  description: string;
  followers_count: number;
  name: string;
  profile_image_url: string;
  screen_name: string;
  tweets_count: number;
  url: string;
}
