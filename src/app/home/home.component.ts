import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import gql from 'graphql-tag';
import {Apollo, QueryRef} from 'apollo-angular';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from './User';
const TWEETER_QUERY = gql`
  query graphQLHub ($identity: UserIdentity!) {
  twitter  {
    user (identifier: name, identity: $identity){
      created_at
      description
      id
      screen_name
      name
      profile_image_url
      url
      tweets_count
      followers_count
      tweets(limit: 10) {
        text
      }
    }
  }
  }
`;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private query: QueryRef<any>;
  private userName = ''
  private tweets: any[] = [];
  private user = new  User();


  constructor(public auth: AuthService, private route: Router, private activatedRoute: ActivatedRoute, private apollo: Apollo) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.userName = params['username'];
     /* if (!this.auth.isAuthenticated()) {
        this.route.navigate(['/login']);
      } else*/
        if (this.userName) {
        this.getTweets();
      }
    });
    // this.getTweets();
  }

  submit() {
    console.log(this.userName);
    this.getTweets();
    return '';
  }

  private getTweets() {
    this.tweets = [];
    this.user = new User();
    this.query = this.apollo.watchQuery({
      query: TWEETER_QUERY,
      variables: { identity: this.userName}
    });

    this.query.valueChanges.subscribe(result => {
      this.tweets = result.data.twitter.user.tweets;
      this.user.description = result.data.twitter.user.description;
      this.user.followers_count = result.data.twitter.user.followers_count;
      this.user.name = result.data.twitter.user.name;
      this.user.profile_image_url = result.data.twitter.user.profile_image_url;
      this.user.screen_name = result.data.twitter.user.screen_name;
      this.user.tweets_count = result.data.twitter.user.tweets_count;
      this.user.url = result.data.twitter.user.url;
    });
  }
}
